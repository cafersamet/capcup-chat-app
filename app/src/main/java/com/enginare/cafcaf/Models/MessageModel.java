package com.enginare.cafcaf.Models;

public class MessageModel {
    private String date;
    private String text;
    private String type;
    private Boolean seen;
    private String from;

    public MessageModel() {
    }

    public MessageModel(String date, String text, String type, Boolean seen, String from) {
        this.date = date;
        this.text = text;
        this.type = type;
        this.seen = seen;
        this.from = from;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    @Override
    public String toString() {
        return "MessageModel{" +
                "date='" + date + '\'' +
                ", text='" + text + '\'' +
                ", type='" + type + '\'' +
                ", seen=" + seen +
                ", from='" + from + '\'' +
                '}';
    }
}
