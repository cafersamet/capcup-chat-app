package com.enginare.cafcaf.Models;

/**
 * Created by CAF on 10/17/2018.
 */

public class User {
    private String name;
    private String telephone;
    private String image;
    private String education;
    private int numberOfFriends;
    private String birthdate;
    private String summary;

    public User() {
    }

    public User(String name, String telephone, String image, String education, int numberOfFriends, String birthdate, String summary) {
        this.name = name;
        this.telephone = telephone;
        this.image = image;
        this.education = education;
        this.numberOfFriends = numberOfFriends;
        this.birthdate = birthdate;
        this.summary = summary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public int getNumberOfFriends() {
        return numberOfFriends;
    }

    public void setNumberOfFriends(int numberOfFriends) {
        this.numberOfFriends = numberOfFriends;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", telephone='" + telephone + '\'' +
                ", image='" + image + '\'' +
                ", education='" + education + '\'' +
                ", numberOfFriends=" + numberOfFriends +
                ", birthdate='" + birthdate + '\'' +
                ", summary='" + summary + '\'' +
                '}';
    }
}
