package com.enginare.cafcaf.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.enginare.cafcaf.Adapters.MessagesAdapter;
import com.enginare.cafcaf.Models.MessageModel;
import com.enginare.cafcaf.Other.DateGenerator;
import com.enginare.cafcaf.Other.FirebaseTables;
import com.enginare.cafcaf.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private String username = "", userID = "";
    private ImageView sendButton;
    private EditText messageInput;
    private RecyclerView recyclerView;
    private DatabaseReference databaseReference;
    private FirebaseUser firebaseUser;
    private List<MessageModel> messageList;
    private MessagesAdapter messagesAdapter;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            username = extras.getString("username");
            userID = extras.getString("userID");
        }

        defineViewsAndObjects();

        retrieveMessages();
    }

    private void retrieveMessages() {
        databaseReference.child(firebaseUser.getUid()).child(userID).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                MessageModel messageModel = dataSnapshot.getValue(MessageModel.class);
                messageList.add(messageModel);
                messagesAdapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(messageList.size() - 1);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void defineViewsAndObjects() {
        messageList = new ArrayList<>();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference(FirebaseTables.messagesTable);
        toolbar = findViewById(R.id.chat_toolbar);
        setSupportActionBar(toolbar);
        if (username.equals("")) {
            username = "Unknown User";
        }
        getSupportActionBar().setTitle(username);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                        overridePendingTransition(R.anim.anim_in_reverse, R.anim.anim_out_reverse);
                    }
                }
        );

        recyclerView = findViewById(R.id.chat_recycler_view);
        messagesAdapter = new MessagesAdapter(messageList, "", getApplicationContext(), this);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);

        recyclerView.setAdapter(messagesAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);


        sendButton = findViewById(R.id.chat_send_button);
        sendButton.setEnabled(false);

        messageInput = findViewById(R.id.chat_message_input);
        messageInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals("")) {
                    sendButton.setEnabled(false);
                } else {
                    sendButton.setEnabled(true);
                }
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage(firebaseUser.getUid(), userID, "0", DateGenerator.getDate(), false, messageInput.getText().toString());
            }
        });
    }

    private void sendMessage(final String myID, final String friendID, String messageType, String date, Boolean seen, String messageText) {
        messageInput.setText("");
        final String messageID = databaseReference.child(myID).child(friendID).push().getKey();
        final Map messageMap = new HashMap<>();
        messageMap.put(FirebaseTables.messagesTableType, messageType);
        messageMap.put(FirebaseTables.messagesTableSeen, seen);
        messageMap.put(FirebaseTables.messagesTableDate, date);
        messageMap.put(FirebaseTables.messagesTableText, messageText);
        messageMap.put(FirebaseTables.messagesTableFrom, myID);

        databaseReference.child(myID).child(friendID).child(messageID).setValue(messageMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    databaseReference.child(friendID).child(myID).child(messageID).setValue(messageMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(ChatActivity.this, "Message could not be sent!*", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    Toast.makeText(ChatActivity.this, "Message could not be sent!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
/*
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {

            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }*/
}
