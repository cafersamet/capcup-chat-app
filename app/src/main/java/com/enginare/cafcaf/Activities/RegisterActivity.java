package com.enginare.cafcaf.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.enginare.cafcaf.Other.FirebaseTables;
import com.enginare.cafcaf.Other.ProgressDialogGenerator;
import com.enginare.cafcaf.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    EditText inputName, inputEmail, inputPassword, inputTel;
    Button registerButton;
    FirebaseAuth auth;
    DatabaseReference databaseReference;
    String extras = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            extras = bundle.getString("intent");
        }

        define();
    }

    private void define() {
        auth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        inputName = findViewById(R.id.input_name);
        inputEmail = findViewById(R.id.input_email);
        inputPassword = findViewById(R.id.input_password);
        inputTel = findViewById(R.id.input_tel);

        registerButton = findViewById(R.id.registerButton);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = inputEmail.getText().toString();
                String password = inputPassword.getText().toString();
                String tel = inputTel.getText().toString();
                String name = inputName.getText().toString();
                register(email, password, tel, name);
            }
        });
    }

    private void register(String email, String password, final String tel, final String name) {
        if (!email.equals("") && !password.equals("") && !tel.equals("") && !name.equals("")) {
            final ProgressDialogGenerator progressDialogGenerator = new ProgressDialogGenerator(this);
            progressDialogGenerator.createProgress("Kayıt gerçekleştiriliyor... ", false);

            auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        databaseReference = databaseReference.child(FirebaseTables.userTable).child(auth.getCurrentUser().getUid());
                        Map map = new HashMap<>();
                        map.put("image", "null");
                        map.put("name", name);
                        map.put("telephone", tel);
                        map.put("education", "");
                        map.put("birthdate", "");
                        map.put("numberOfFriends", 0);
                        map.put("summary", "");

                        databaseReference.setValue(map);

                        Toast.makeText(RegisterActivity.this, "Kayıt gerçekleşti", Toast.LENGTH_SHORT).show();
                        progressDialogGenerator.cancel();
                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
                        finish();
                    } else {
                        try {
                            Toast.makeText(RegisterActivity.this, task.getResult().toString(), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            Toast.makeText(RegisterActivity.this, "Kayıt gerçekleşmedi...", Toast.LENGTH_SHORT).show();
                        }
                        progressDialogGenerator.cancel();
                    }
                }
            });
        } else {
            Toast.makeText(this, "Check empty fields", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (extras.equals("login")) {
            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.anim_in_reverse, R.anim.anim_out_reverse);
        }
    }
}
