package com.enginare.cafcaf.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.enginare.cafcaf.Other.ProgressDialogGenerator;
import com.enginare.cafcaf.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;

public class LoginActivity extends AppCompatActivity {

    private EditText emailEditText, passwordEditText;
    private Button loginButton, createAccountButton;
    private TextView forgetPasswordText;
    private FirebaseAuth auth;
    private ProgressDialogGenerator progressDialogGenerator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        define();
    }

    private void define() {
        auth = FirebaseAuth.getInstance();
        emailEditText = findViewById(R.id.login_input_email);
        passwordEditText = findViewById(R.id.login_input_password);
        forgetPasswordText = findViewById(R.id.forgetPasswordTextView);
        forgetPasswordText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //şifre sıfırlama
            }
        });

        loginButton = findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailEditText.getText().toString();
                String password = passwordEditText.getText().toString();

                if (!email.equals("") && !password.equals("")) {
                    startLogin(email, password);
                } else {
                    Toast.makeText(LoginActivity.this, "Check empty fields", Toast.LENGTH_SHORT).show();
                }
            }
        });

        createAccountButton = findViewById(R.id.createAccountButton);
        createAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                intent.putExtra("intent", "login");
                startActivity(intent);
                overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
                finish();
            }
        });
    }

    private void startLogin(String email, String password) {
        progressDialogGenerator = new ProgressDialogGenerator(this);
        progressDialogGenerator.createProgress("Giriş yapılıyor...", false);
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(LoginActivity.this, "Giriş yapıldı", Toast.LENGTH_SHORT).show();
                    progressDialogGenerator.cancel();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
                    finish();
                } else {
                    Toast.makeText(LoginActivity.this, "Giriş yapılamadı...", Toast.LENGTH_SHORT).show();
                    progressDialogGenerator.cancel();
                    try {
                        throw task.getException();
                    } catch (FirebaseAuthInvalidUserException invalidUserException) {
                        Toast.makeText(LoginActivity.this, "Bu e-posta adresi sistemimize kayıtlı değildir!", Toast.LENGTH_LONG).show();
                    } catch (FirebaseAuthInvalidCredentialsException invalidEmailException) {
                        Toast.makeText(LoginActivity.this, "Bilgilerinizi kontrol ediniz!", Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
}
