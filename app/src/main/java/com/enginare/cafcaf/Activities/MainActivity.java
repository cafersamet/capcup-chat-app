package com.enginare.cafcaf.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.enginare.cafcaf.Fragments.FriendsFragment;
import com.enginare.cafcaf.Fragments.MainPageFragment;
import com.enginare.cafcaf.Fragments.ProfileFragment;
import com.enginare.cafcaf.Fragments.RequestFragment;
import com.enginare.cafcaf.Other.ChangeFragment;
import com.enginare.cafcaf.Other.FirebaseTables;
import com.enginare.cafcaf.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    private FirebaseUser firebaseUser;
    private ChangeFragment changeFragment;
    private int currentFragment = 0;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        define();

        if (!checkUserAvailable()) {
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
            finish();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        databaseReference.child(firebaseUser.getUid()).setValue(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        databaseReference.child(firebaseUser.getUid()).setValue(false);
    }

    private void define() {
        changeFragment = new ChangeFragment(MainActivity.this);
        changeFragment.change(new MainPageFragment(), R.id.mainFragment);
        currentFragment = R.id.navigation_home;

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference(FirebaseTables.statusTable);
    }

    private boolean checkUserAvailable() {
        if (firebaseUser == null) {
            return false;
        } else {
            return true;
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    //if (currentFragment != item.getItemId()) {
                    changeFragment.change(new MainPageFragment(), R.id.mainFragment);
                    //}
                    currentFragment = item.getItemId();
                    return true;
                case R.id.navigation_chat:
                    //if (currentFragment != item.getItemId()) {
                    changeFragment.change(new FriendsFragment(), R.id.mainFragment);
                    //}
                    currentFragment = item.getItemId();
                    return true;
                case R.id.navigation_requests:
                    //if (currentFragment != item.getItemId()) {
                    changeFragment.change(new RequestFragment(), R.id.mainFragment);
                    //}
                    currentFragment = item.getItemId();
                    return true;
                case R.id.navigation_signout:
                    // (currentFragment != item.getItemId()) {
                    changeFragment.change(new ProfileFragment(), R.id.mainFragment);
                    //}
                    currentFragment = item.getItemId();
                    return true;
            }
            return false;
        }
    };

}
