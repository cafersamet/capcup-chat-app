package com.enginare.cafcaf.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.enginare.cafcaf.Models.MessageModel;
import com.enginare.cafcaf.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder> {
    private Context context;
    private List<MessageModel> messageList;
    private String userID;
    private String myID = "";
    private FirebaseDatabase firebaseDatabase;
    private Activity activity;
    private Boolean state;
    private int VIEW_TYPE_SENT = 1, VIEW_TYPE_RECEIVED = 2;

    public MessagesAdapter(List<MessageModel> messageList, String userID, Context context, Activity activity) {
        this.messageList = messageList;
        this.context = context;
        this.activity = activity;
        this.userID = userID;
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null)
            myID = firebaseUser.getUid();
        firebaseDatabase = FirebaseDatabase.getInstance();
        state = false;
    }

    @NonNull
    @Override
    public MessagesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        if (i == VIEW_TYPE_SENT) {
            view = LayoutInflater.from(context).inflate(R.layout.message_sent_layout, viewGroup, false);
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.message_received_layout, viewGroup, false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MessagesAdapter.ViewHolder viewHolder, final int i) {
        Log.i("Message",messageList.get(i).getText());
        viewHolder.messageText.setText(messageList.get(i).getText());
    }

    @Override
    public int getItemViewType(int position) {
        if (messageList.get(position).getFrom().equals(myID)) {
            state = true;
            return VIEW_TYPE_SENT;
        } else {
            state = false;
            return VIEW_TYPE_RECEIVED;
        }
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView messageText;
        ConstraintLayout messageLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            if (state) {
                messageText = itemView.findViewById(R.id.message_sent_text);
            } else {
                messageText = itemView.findViewById(R.id.message_received_text);
            }

        }
    }
}
