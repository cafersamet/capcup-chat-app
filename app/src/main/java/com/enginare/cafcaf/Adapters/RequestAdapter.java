package com.enginare.cafcaf.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.enginare.cafcaf.Fragments.FriendProfileFragment;
import com.enginare.cafcaf.Models.User;
import com.enginare.cafcaf.Other.ChangeFragment;
import com.enginare.cafcaf.Other.FirebaseTables;
import com.enginare.cafcaf.Other.ProgressDialogGenerator;
import com.enginare.cafcaf.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder> {

    private List<User> userList;
    private Context context;
    private List<String> userIDList;
    private String myID = "";
    private FirebaseDatabase firebaseDatabase;
    private ProgressDialogGenerator progressDialogGenerator;
    private Activity activity;

    public RequestAdapter(List<User> userList, Context context, List<String> userIDList, Activity activity) {
        this.userList = userList;
        this.context = context;
        this.userIDList = userIDList;
        this.activity = activity;
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null)
            myID = firebaseUser.getUid();
        firebaseDatabase = FirebaseDatabase.getInstance();
        progressDialogGenerator = new ProgressDialogGenerator(this.activity);
    }

    @NonNull
    @Override
    public RequestAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.request_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RequestAdapter.ViewHolder viewHolder, final int i) {
        Picasso.get().load(userList.get(i).getImage()).placeholder(R.drawable.default_profile_male).into(viewHolder.profileImage);
        viewHolder.nameText.setText(userList.get(i).getName());
        viewHolder.acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptRequest(userIDList.get(i), i);
            }
        });

        viewHolder.rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rejectRequest(userIDList.get(i), i);
            }
        });

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeFragment changeFragment = new ChangeFragment(context);
                changeFragment.change(new FriendProfileFragment(), R.id.mainFragment, userIDList.get(i));
            }
        });
    }

    private void rejectRequest(final String friendID, final int i) {
        final DatabaseReference databaseReference = firebaseDatabase.getReference(FirebaseTables.requestTable);
        progressDialogGenerator.createProgress("İstek iptal ediliyor...", false);
        databaseReference.child(myID).child(friendID).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                databaseReference.child(friendID).child(myID).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        progressDialogGenerator.cancel();
                        userIDList.remove(i);
                        userList.remove(i);
                        notifyDataSetChanged();
                    }
                });
            }
        });
    }

    private void acceptRequest(final String friendID, final int i) {
        Calendar calendar = Calendar.getInstance();
        final String date = calendar.get(Calendar.DAY_OF_MONTH) +
                "/" + calendar.get(Calendar.MONTH) +
                "/" + calendar.get(Calendar.YEAR);
        final DatabaseReference friendshipDatabaseReference = firebaseDatabase.getReference(FirebaseTables.friendshipTable);
        friendshipDatabaseReference.child(myID).child(friendID).child(FirebaseTables.friendshipTableDate).setValue(date).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                friendshipDatabaseReference.child(friendID).child(myID).child(FirebaseTables.friendshipTableDate).setValue(date).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        final DatabaseReference requestDatabaseReference = firebaseDatabase.getReference(FirebaseTables.requestTable);
                        requestDatabaseReference.child(myID).child(friendID).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                requestDatabaseReference.child(friendID).child(myID).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        userIDList.remove(i);
                                        userList.remove(i);
                                        notifyDataSetChanged();
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView profileImage;
        TextView nameText;
        ImageButton acceptButton, rejectButton;
        CardView cardView;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.request_card_view);
            profileImage = itemView.findViewById(R.id.request_image_view);
            nameText = itemView.findViewById(R.id.request_name);
            acceptButton = itemView.findViewById(R.id.request_accept_button);
            rejectButton = itemView.findViewById(R.id.request_reject_button);
        }
    }
}
