package com.enginare.cafcaf.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.enginare.cafcaf.Fragments.FriendProfileFragment;
import com.enginare.cafcaf.Models.User;
import com.enginare.cafcaf.Other.ChangeFragment;
import com.enginare.cafcaf.Other.FirebaseTables;
import com.enginare.cafcaf.Other.ProgressDialogGenerator;
import com.enginare.cafcaf.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by CAF on 10/19/2018.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private List<String> userKeyList;
    private Context context;
    private Activity activity;
    private DatabaseReference databaseReference;
    private FirebaseUser firebaseUser;
    private StorageReference storageReference;
    private ProgressDialogGenerator progressDialogGenerator;


    public UserAdapter(List<String> userKeyList, Context context, Activity activity) {
        this.userKeyList = userKeyList;
        this.context = context;
        this.activity = activity;
        databaseReference = FirebaseDatabase.getInstance().getReference();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        storageReference = FirebaseStorage.getInstance().getReference();
        progressDialogGenerator = new ProgressDialogGenerator(activity);
    }

    //Layout tanımlası yapılır
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_layout, viewGroup, false);
        //progressDialogGenerator.createProgress("Kullanıcılar listeleniyor...", false);
        return new ViewHolder(view);
    }

    //View lar set edilir
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        //viewHolder.userNameTextView.setText(userKeyList.get(i).toString());
        databaseReference.child(FirebaseTables.userTable).child(userKeyList.get(i)).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final User user = dataSnapshot.getValue(User.class);
                Picasso.get().load(user.getImage()).placeholder(R.drawable.default_profile_male).into(viewHolder.userProfileImage);
                viewHolder.userNameTextView.setText(user.getName());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeFragment changeFragment = new ChangeFragment(context);
                changeFragment.change(new FriendProfileFragment(), R.id.mainFragment, userKeyList.get(i));
            }
        });
    }

    //adapter i oluşturulacak listenin size ı
    @Override
    public int getItemCount() {
        return userKeyList.size();
    }

    //view ların tanımlanması
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView userNameTextView;
        CircleImageView userProfileImage;
        CardView cardView;

        ViewHolder(View itemView) {
            super(itemView);
            userNameTextView = itemView.findViewById(R.id.mainUserListName);
            userProfileImage = itemView.findViewById(R.id.mainUserListImage);
            cardView = itemView.findViewById(R.id.user_card_view);
        }
    }
}
