package com.enginare.cafcaf.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.enginare.cafcaf.Activities.ChatActivity;
import com.enginare.cafcaf.Models.User;
import com.enginare.cafcaf.Other.FirebaseTables;
import com.enginare.cafcaf.Other.ProgressDialogGenerator;
import com.enginare.cafcaf.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder> {

    private Context context;
    private List<String> userIDList;
    private String myID = "";
    private DatabaseReference databaseReference;
    private ProgressDialogGenerator progressDialogGenerator;
    private Activity activity;

    public FriendsAdapter(Context context, List<String> userIDList, Activity activity) {
        this.context = context;
        this.userIDList = userIDList;
        this.activity = activity;
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null)
            myID = firebaseUser.getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        progressDialogGenerator = new ProgressDialogGenerator(this.activity);
    }

    @NonNull
    @Override
    public FriendsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.friend_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FriendsAdapter.ViewHolder viewHolder, final int i) {
        databaseReference.child(FirebaseTables.userTable).child(userIDList.get(i)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    final User user = dataSnapshot.getValue(User.class);
                    if (user != null) {
                        Picasso.get().load(user.getImage()).placeholder(R.drawable.default_profile_male).into(viewHolder.profileImage);
                        viewHolder.nameText.setText(user.getName());
                        viewHolder.cardView.setVisibility(View.VISIBLE);
                        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(activity, ChatActivity.class);
                                intent.putExtra("username", user.getName());
                                intent.putExtra("userID", dataSnapshot.getKey());
                                activity.startActivity(intent);
                                activity.overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
                            }
                        });
                    } else {
                        Toast.makeText(context, "Bir kullanıcının bilgileri ile ilgili sorun var!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(context, "Bir kullanıcının bilgilerine ulaşılamadı!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, "Bir kullanıcının bilgilerine ulaşılırken sorun oluştu!", Toast.LENGTH_LONG).show();
            }
        });

        databaseReference.child(FirebaseTables.statusTable).child(userIDList.get(i)).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    boolean status = (boolean) dataSnapshot.getValue();
                    if (status) {
                        viewHolder.statusIcon.setImageResource(R.drawable.green_circle_view);
                        viewHolder.statusText.setText("Online");
                    } else {
                        viewHolder.statusIcon.setImageResource(R.drawable.red_circle_view);
                        viewHolder.statusText.setText("Offline");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                viewHolder.statusIcon.setImageResource(R.drawable.red_circle_view);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userIDList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView profileImage;
        TextView nameText, statusText;
        ImageView statusIcon;
        CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.friends_card_view);
            cardView.setVisibility(View.GONE);
            profileImage = itemView.findViewById(R.id.friends_list_image);
            nameText = itemView.findViewById(R.id.friends_list_name);
            statusText = itemView.findViewById(R.id.friends_list_status_text);
            statusIcon = itemView.findViewById(R.id.friends_list_status_icon);
        }
    }
}
