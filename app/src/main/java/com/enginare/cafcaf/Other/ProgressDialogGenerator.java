package com.enginare.cafcaf.Other;

import android.app.Activity;
import android.app.ProgressDialog;

/**
 * Created by CAF on 10/17/2018.
 */

public class ProgressDialogGenerator {
    private Activity activity;
    private ProgressDialog progressDialog;

    public ProgressDialogGenerator(Activity activity) {
        this.activity = activity;
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public void setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }

    public void createProgress(String title, String message, boolean cancelable){
        progressDialog = new ProgressDialog(activity);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(cancelable);
        progressDialog.show();
    }

    public void createProgress(String message, boolean cancelable){
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(cancelable);
        progressDialog.show();
    }

    public void cancel(){
        progressDialog.cancel();
    }
}
