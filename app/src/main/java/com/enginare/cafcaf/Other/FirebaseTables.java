package com.enginare.cafcaf.Other;

/**
 * Created by CAF on 10/17/2018.
 */

public class FirebaseTables {
    public static final String userTable = "users";
    public static final String userTableName = "name";
    public static final String userTableTelephone = "telephone";
    public static final String userTableEducation = "education";
    public static final String userTableBirthdate = "birthdate";
    public static final String userTableSummary = "summary";
    public static final String userTableImage = "image";
    public static final String userTableNoOfFriends = "numberOfFriends";

    public static final String profileImageTable = "profile_image";

    public static final String requestTable = "requests";
    public static final String requestTableType = "type";
    public static final String requestTableTypeSent = "sent";
    public static final String requestTableTypeReceived = "received";

    public static final String friendshipTable = "friendship";
    public static final String friendshipTableDate = "date";

    public static final String statusTable = "status";

    public static final String messagesTable = "messages";
    public static final String messagesTableType = "type";
    public static final String messagesTableSeen = "seen";
    public static final String messagesTableDate = "date";
    public static final String messagesTableText = "text";
    public static final String messagesTableFrom = "from";
}
