package com.enginare.cafcaf.Other;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateGenerator {
    public static String getDate() {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date currentDate = Calendar.getInstance().getTime();
        return df.format(currentDate);
    }
}
