package com.enginare.cafcaf.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.enginare.cafcaf.Adapters.FriendsAdapter;
import com.enginare.cafcaf.Models.User;
import com.enginare.cafcaf.Other.FirebaseTables;
import com.enginare.cafcaf.Other.ProgressDialogGenerator;
import com.enginare.cafcaf.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FriendsFragment extends Fragment {

    private View view;
    private List<User> userList;
    private List<String> userIDList;
    private FirebaseDatabase database;
    private ProgressDialogGenerator progressDialogGenerator;
    private FriendsAdapter friendsAdapter;
    private RecyclerView recyclerView;
    private TextView resultText;
    private LinearLayoutManager linearLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_friends, container, false);
        define();
        showRequests();
        return view;
    }

    private void define() {
        userIDList = new ArrayList<>();
        userList = new ArrayList<>();
        database = FirebaseDatabase.getInstance();
        progressDialogGenerator = new ProgressDialogGenerator(getActivity());
        friendsAdapter = new FriendsAdapter(getContext(), userIDList, getActivity());
        resultText = view.findViewById(R.id.friends_result_text);
        recyclerView = view.findViewById(R.id.friends_recycler_view);
        linearLayoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(friendsAdapter);
    }

    private void showRequests() {
        DatabaseReference databaseReference = database.getReference(FirebaseTables.friendshipTable);
        progressDialogGenerator.createProgress("Bildirimler alınıyor...", false);
        String myID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        databaseReference.child(myID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot friend : dataSnapshot.getChildren()) {
                        userIDList.add(friend.getKey());
                        friendsAdapter.notifyDataSetChanged();
                    }
                } else {
                    resultText.setVisibility(View.VISIBLE);
                }
                if (progressDialogGenerator.getProgressDialog().isShowing())
                    progressDialogGenerator.cancel();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressDialogGenerator.cancel();
                Toast.makeText(getActivity(), "Arkadaşlarınıza ulaşılamadı!", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
