package com.enginare.cafcaf.Fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.enginare.cafcaf.Models.User;
import com.enginare.cafcaf.Other.DateGenerator;
import com.enginare.cafcaf.Other.FirebaseTables;
import com.enginare.cafcaf.Other.ProgressDialogGenerator;
import com.enginare.cafcaf.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

public class FriendProfileFragment extends Fragment {

    private View view;
    private String userID; // id of user whose profile will be showed
    private ImageView profileImageView;
    private Button messageButton, addFriendButton;
    private TextView nameText, phoneText, educationText, numberOfFriendsText, birthdateText, summaryText;
    private FirebaseDatabase database;
    private ProgressDialogGenerator progressDialogGenerator;
    private FirebaseUser firebaseUser;
    private Map<String, String> friendDateMap;

    private String CANCEL_REQUEST = "CANCEL REQUEST";
    private String ADD_FRIEND = "ADD FRIEND";
    private String CONFIRM_REQUEST = "CONFIRM REQUEST";
    private String FRIENDS = "FRIENDS";


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_friend_profile, container, false);
        define();
        return view;
    }

    private void retrieveUserInformation() {
        progressDialogGenerator.createProgress("Bilgiler alınıyor...", false);
        DatabaseReference databaseReference = database.getReference(FirebaseTables.userTable).child(userID);
        // get user information
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    User user = dataSnapshot.getValue(User.class);
                    if (user != null) {
                        Picasso.get().load(user.getImage()).placeholder(R.drawable.default_profile_male).into(profileImageView);
                        nameText.setText(user.getName());
                        phoneText.setText(user.getTelephone());

                        if (!user.getEducation().equals("")) {
                            educationText.setText(user.getEducation());
                        } else {
                            educationText.setText("Education not specified");
                        }

                        if (!user.getBirthdate().equals("")) {
                            birthdateText.setText(user.getBirthdate());
                        } else {
                            birthdateText.setText("Birthdate not specified");
                        }
                        numberOfFriendsText.setText(String.valueOf(user.getNumberOfFriends()));
                        DatabaseReference ref = database.getReference(FirebaseTables.userTable).child(firebaseUser.getUid());
                        ref.child(FirebaseTables.userTableNoOfFriends).setValue(friendDateMap.size()).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getActivity(), "Arkadaş sayısı güncellenirken problem oluştu.", Toast.LENGTH_SHORT).show();
                            }
                        });

                        if (!user.getSummary().equals("")) {
                            summaryText.setText(user.getSummary());
                        } else {
                            summaryText.setText("No summary");
                        }

                        DatabaseReference databaseReference = database.getReference(FirebaseTables.friendshipTable);
                        String myID = firebaseUser.getUid();
                        // get friendship information (how many friends does user have)
                        databaseReference.child(myID).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    friendDateMap.clear();
                                    for (DataSnapshot friend : dataSnapshot.getChildren()) {
                                        String key = friend.getKey();
                                        String date = friend.child(FirebaseTables.friendshipTableDate).getValue().toString();
                                        friendDateMap.put(key, date);
                                    }
                                    if (!friendDateMap.isEmpty()) {
                                        //numberOfFriendsText.setText(String.valueOf(friendDateMap.size()));
                                        DatabaseReference ref = database.getReference(FirebaseTables.userTable).child(firebaseUser.getUid());
                                        ref.child(FirebaseTables.userTableNoOfFriends).setValue(friendDateMap.size()).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Toast.makeText(getActivity(), "Arkadaş sayısı güncellenirken problem oluştu.", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                } else {
                                    addFriendButton.setText(ADD_FRIEND);
                                    numberOfFriendsText.setText("0");
                                    DatabaseReference ref = database.getReference(FirebaseTables.userTable).child(firebaseUser.getUid());
                                    ref.child(FirebaseTables.userTableNoOfFriends).setValue(0).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(getActivity(), "Arkadaş sayısı güncellenirken problem oluştu.", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                    if (!friendDateMap.isEmpty()) {
                                        friendDateMap.remove(userID);
                                    }
                                }
                                // user is not friend with current profile
                                if (!friendDateMap.containsKey(userID)) {
                                    DatabaseReference ref = database.getReference(FirebaseTables.requestTable);
                                    // check requests
                                    final int[] counter = {0};
                                    ref.child(firebaseUser.getUid()).child(userID).child(FirebaseTables.requestTableType).addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {
                                                Log.i("COUNTER", String.valueOf(++counter[0]));
                                                Log.i("0", "0");
                                                String type = dataSnapshot.getValue().toString();
                                                switch (type) {
                                                    case FirebaseTables.requestTableTypeSent:
                                                        addFriendButton.setText(CANCEL_REQUEST);
                                                        break;
                                                    case FirebaseTables.requestTableTypeReceived:
                                                        addFriendButton.setText(CONFIRM_REQUEST);
                                                        break;
                                                    default:
                                                        addFriendButton.setText(ADD_FRIEND);
                                                        break;
                                                }
                                                if (progressDialogGenerator.getProgressDialog().isShowing())
                                                    progressDialogGenerator.cancel();
                                            } else {
                                                Log.i("1", "1");
                                                if (progressDialogGenerator.getProgressDialog().isShowing())
                                                    progressDialogGenerator.cancel();
                                                if (!friendDateMap.containsKey(userID)) {
                                                    addFriendButton.setText(ADD_FRIEND);
                                                } else {
                                                    addFriendButton.setText(FRIENDS);
                                                }

                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            progressDialogGenerator.cancel();
                                            addFriendButton.setText(ADD_FRIEND);
                                            Toast.makeText(getActivity(), "Request information could not be received!", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                } else {
                                    Log.i("2", "2");
                                    if (progressDialogGenerator.getProgressDialog().isShowing())
                                        progressDialogGenerator.cancel();
                                    addFriendButton.setText(FRIENDS);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                if (progressDialogGenerator.getProgressDialog().isShowing())
                                    progressDialogGenerator.cancel();
                                Toast.makeText(getActivity(), "Arkadaş sayısı ile ilgili bir hata oluştu!", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        progressDialogGenerator.cancel();
                        Toast.makeText(getActivity(), "*User not found!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    progressDialogGenerator.cancel();
                    Toast.makeText(getActivity(), "User not found!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                if (progressDialogGenerator.getProgressDialog().isShowing())
                    progressDialogGenerator.cancel();
                Toast.makeText(getActivity(), "Bir hata oluştu!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addFriend(final String myID, final String friendID) {
        final DatabaseReference databaseReference = database.getReference(FirebaseTables.requestTable);
        progressDialogGenerator.createProgress("İstek gönderiliyor...", false);
        databaseReference.child(myID).child(friendID).child(FirebaseTables.requestTableType).setValue(FirebaseTables.requestTableTypeSent)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            databaseReference.child(friendID).child(myID).child(FirebaseTables.requestTableType).setValue(FirebaseTables.requestTableTypeReceived)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                addFriendButton.setText(CANCEL_REQUEST);
                                            } else {
                                                Toast.makeText(getActivity(), "*Request could not be sent!",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                            progressDialogGenerator.cancel();
                                        }
                                    });


                        } else {
                            Toast.makeText(getActivity(), "Request could not be sent!",
                                    Toast.LENGTH_SHORT).show();
                            progressDialogGenerator.cancel();
                        }
                    }
                });
    }

    public void define() {
        database = FirebaseDatabase.getInstance();
        progressDialogGenerator = new ProgressDialogGenerator(getActivity());
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        friendDateMap = new HashMap<>();
        if (getArguments() != null) {
            userID = getArguments().getString("userID");
            profileImageView = view.findViewById(R.id.friend_profile_image);
            nameText = view.findViewById(R.id.friend_profile_name_text);
            phoneText = view.findViewById(R.id.friend_profile_phone_text);
            educationText = view.findViewById(R.id.friend_profile_education_text);
            numberOfFriendsText = view.findViewById(R.id.friend_profile_number_text);
            birthdateText = view.findViewById(R.id.profile_friend_birthdate_text);
            summaryText = view.findViewById(R.id.friend_profile_summary_text);
            messageButton = view.findViewById(R.id.friend_profile_message_button);
            addFriendButton = view.findViewById(R.id.friend_profile_add_friend_button);

            messageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            addFriendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String buttonText = ((Button) v).getText().toString().toUpperCase();
                    Log.e("Button text", buttonText);
                    String myID = firebaseUser.getUid();
                    if (buttonText.equals(ADD_FRIEND)) {
                        addFriend(myID, userID);
                    } else if (buttonText.equals(CANCEL_REQUEST)) {
                        cancelRequest(myID, userID);
                    } else if (buttonText.equals(CONFIRM_REQUEST)) {
                        confirmRequest(myID, userID);
                    } else {
                        showDialogForUnfriend(myID);
                    }
                }
            });

            retrieveUserInformation();
        } else {
            Toast.makeText(getActivity(), "Seçilen kullanıcı algılanamadı!", Toast.LENGTH_SHORT).show();
        }
    }

    private void showDialogForUnfriend(final String myID) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Arkadaşlık")
                .setMessage("Arkadaşlıktan çıkarmak istediğinize emin misiniz?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        unfriendAction(myID, userID);
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                })
                .setCancelable(true)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void unfriendAction(final String myID, final String friendID) {
        final DatabaseReference databaseReference = database.getReference(FirebaseTables.friendshipTable);
        progressDialogGenerator.createProgress("...", false);
        databaseReference.child(myID).child(friendID).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                databaseReference.child(friendID).child(myID).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        addFriendButton.setText(ADD_FRIEND);
                        progressDialogGenerator.cancel();
                    }
                });
            }
        });
    }

    private void confirmRequest(final String myID, final String friendID) {
        final String date = DateGenerator.getDate();

        final DatabaseReference friendshipDatabaseReference = database.getReference(FirebaseTables.friendshipTable);
        progressDialogGenerator.createProgress("", false);
        friendshipDatabaseReference.child(myID).child(friendID).child(FirebaseTables.friendshipTableDate).setValue(date).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                friendshipDatabaseReference.child(friendID).child(myID).child(FirebaseTables.friendshipTableDate).setValue(date).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        final DatabaseReference requestDatabaseReference = database.getReference(FirebaseTables.requestTable);
                        requestDatabaseReference.child(myID).child(friendID).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                requestDatabaseReference.child(friendID).child(myID).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        addFriendButton.setText(FRIENDS);
                                        progressDialogGenerator.cancel();
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    private void cancelRequest(final String myID, final String friendID) {
        final DatabaseReference databaseReference = database.getReference(FirebaseTables.requestTable);
        progressDialogGenerator.createProgress("İstek gönderiliyor...", false);
        databaseReference.child(myID).child(friendID).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                databaseReference.child(friendID).child(myID).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        addFriendButton.setText(ADD_FRIEND);
                        progressDialogGenerator.cancel();
                    }
                });
            }
        });
    }

}
