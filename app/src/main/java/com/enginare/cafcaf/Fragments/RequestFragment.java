package com.enginare.cafcaf.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.enginare.cafcaf.Adapters.RequestAdapter;
import com.enginare.cafcaf.Models.User;
import com.enginare.cafcaf.Other.FirebaseTables;
import com.enginare.cafcaf.Other.ProgressDialogGenerator;
import com.enginare.cafcaf.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class RequestFragment extends Fragment {

    private View view;
    private String myID;
    private List<User> userList;
    private List<String> userIDList;
    private FirebaseDatabase database;
    private ProgressDialogGenerator progressDialogGenerator;
    private RequestAdapter requestAdapter;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private TextView resultText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_request, container, false);
        define();
        showRequests();
        return view;
    }

    private void define() {
        myID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        userList = new ArrayList<>();
        userIDList = new ArrayList<>();
        database = FirebaseDatabase.getInstance();
        progressDialogGenerator = new ProgressDialogGenerator(getActivity());
        resultText = view.findViewById(R.id.request_result_text);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView = view.findViewById(R.id.request_recycler_view);
        requestAdapter = new RequestAdapter(userList, getContext(), userIDList, getActivity());
        recyclerView.setAdapter(requestAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void showRequests() {
        DatabaseReference databaseReference = database.getReference(FirebaseTables.requestTable);
        progressDialogGenerator.createProgress("Bildirimler alınıyor...", false);
        databaseReference.child(myID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (final DataSnapshot request : dataSnapshot.getChildren()) {
                        if (request.getKey() != null && request.child(FirebaseTables.requestTableType).getValue() != null) {
                            Log.i("ID", request.getKey());
                            String requestType = request.child(FirebaseTables.requestTableType).getValue().toString();
                            Log.i("Type", requestType);
                            if (requestType.equals(FirebaseTables.requestTableTypeReceived)) {
                                DatabaseReference ref = database.getReference(FirebaseTables.userTable).child(request.getKey());
                                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            User user = dataSnapshot.getValue(User.class);
                                            if (user != null) {
                                                userList.add(user);
                                                userIDList.add(request.getKey());
                                                requestAdapter.notifyDataSetChanged();
                                                Log.i("ID", request.getKey());
                                                Log.i("User", user.toString());
                                            }
                                        }

                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        progressDialogGenerator.cancel();
                                        Toast.makeText(getActivity(), "Kullanıcı bilgilerine ulaşılamadı!", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }
                    }
                    if (progressDialogGenerator.getProgressDialog().isShowing())
                        progressDialogGenerator.cancel();
                } else {
                    resultText.setVisibility(View.VISIBLE);
                    progressDialogGenerator.cancel();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressDialogGenerator.cancel();
                Toast.makeText(getActivity(), "Bildirimlere ulaşılamadı!", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
