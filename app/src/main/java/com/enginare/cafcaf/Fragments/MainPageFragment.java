package com.enginare.cafcaf.Fragments;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.enginare.cafcaf.Adapters.UserAdapter;
import com.enginare.cafcaf.Models.User;
import com.enginare.cafcaf.Other.FirebaseTables;
import com.enginare.cafcaf.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainPageFragment extends Fragment {

    private View view;
    private DatabaseReference databaseReference;
    private List<String> userKeyList;
    private RecyclerView userListRecyclerView;
    private UserAdapter userAdapter;
    FirebaseUser firebaseUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_main_page, container, false);

        define();
        getUserKeys();

        return view;
    }

    private void define() {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        userKeyList = new ArrayList<>();
        userListRecyclerView = view.findViewById(R.id.userListRecyclerView);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        userListRecyclerView.setLayoutManager(manager);

        //userListRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));

        userAdapter = new UserAdapter(userKeyList, getContext(), getActivity());
        userListRecyclerView.setAdapter(userAdapter);
    }

    private void getUserKeys() {
        databaseReference.child(FirebaseTables.userTable).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                        User user = userSnapshot.getValue(User.class);
                        if (user != null && user.getName() != null && !user.getName().equals("null") && !userSnapshot.getKey().equals(firebaseUser.getUid())) {
                            Log.i("USER INFO", user.toString());
                            userKeyList.add(userSnapshot.getKey());
                            userAdapter.notifyDataSetChanged();
                        }
                    }
                } else {
                    Toast.makeText(getActivity(), "Kullanıcı bulunamadı.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public class SimpleDividerItemDecoration extends RecyclerView.ItemDecoration {
        private Drawable mDivider;

        public SimpleDividerItemDecoration(Context context) {
            mDivider = context.getResources().getDrawable(R.drawable.recyclerview_divider);
        }

        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + mDivider.getIntrinsicHeight();

                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }
    }

}
