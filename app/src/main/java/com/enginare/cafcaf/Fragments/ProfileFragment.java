package com.enginare.cafcaf.Fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.enginare.cafcaf.Activities.LoginActivity;
import com.enginare.cafcaf.Models.User;
import com.enginare.cafcaf.Other.ChangeFragment;
import com.enginare.cafcaf.Other.FirebaseTables;
import com.enginare.cafcaf.Other.ImageResizer;
import com.enginare.cafcaf.Other.ProgressDialogGenerator;
import com.enginare.cafcaf.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ProfileFragment extends Fragment {

    private FirebaseAuth auth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;
    private StorageReference storageReference;

    private Calendar calendar;
    private ProgressDialogGenerator progressDialogGenerator;
    private View view;
    private Button saveButton;
    private EditText nameEditText, phoneEditText, educationEditText, birhdateEditText, summaryEditText;
    private ImageView profile_image, signOutImageView;
    private TextView changeProfile;
    private String oldName = "", oldTelephone = "", oldEducation = "", oldBirthdate = "", oldSummary = "";
    private String imageUrl = "null";
    private int noOfFriends;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        define();
        getUserInformation();
        return view;
    }

    private void define() {
        progressDialogGenerator = new ProgressDialogGenerator(getActivity());
        auth = FirebaseAuth.getInstance();
        firebaseUser = auth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebaseTables.userTable).child(firebaseUser.getUid());
        storageReference = FirebaseStorage.getInstance().getReference();

        profile_image = view.findViewById(R.id.profile_image);
        nameEditText = view.findViewById(R.id.profile_name_edit_text);
        phoneEditText = view.findViewById(R.id.profile_phone_edit_text);
        educationEditText = view.findViewById(R.id.profile_education_edit_text);
        birhdateEditText = view.findViewById(R.id.profile_birthdate_edit_text);
        summaryEditText = view.findViewById(R.id.profile_summary_edit_text);

        calendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        // Create datepicker when birthdate edittext clicked
        birhdateEditText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        saveButton = view.findViewById(R.id.profile_saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = nameEditText.getText().toString().trim();
                String telephone = phoneEditText.getText().toString().trim();
                String education = educationEditText.getText().toString().trim();
                String birthdate = birhdateEditText.getText().toString().trim();
                String summary = summaryEditText.getText().toString().trim();

                if (oldTelephone.equals(telephone) && oldName.equals(name) && oldEducation.equals(education)
                        && oldBirthdate.equals(birthdate) && oldSummary.equals(summary)) {
                    Toast.makeText(getActivity(), "Herhangi bir değişiklik yapmadınız...", Toast.LENGTH_LONG).show();
                    ChangeFragment changeFragment = new ChangeFragment(getContext());
                    changeFragment.change(new MainPageFragment(), R.id.mainFragment);
                } else if (name.equals("")) {
                    Toast.makeText(getActivity(), "İsim alanı boş bırakılamaz!", Toast.LENGTH_SHORT).show();
                } else {
                    updateUserInformation(name, telephone, education, birthdate, summary);
                }
            }
        });
        signOutImageView = view.findViewById(R.id.signOut_ImageView);
        signOutImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signOut();
            }
        });

        changeProfile = view.findViewById(R.id.profile_change_TextView);
        changeProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "dd.MM.yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        birhdateEditText.setText(sdf.format(calendar.getTime()));
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 5);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 5 && resultCode == Activity.RESULT_OK) {
            progressDialogGenerator.createProgress("Resim Yükleniyor...", false);
            Uri filePath = data.getData();

            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContext().getContentResolver().query(
                    filePath, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String path = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bitmap = null;
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), filePath);
                bitmap = ImageResizer.modifyOrientation(bitmap, path);
                float maxImageSize = 400;
                if (!(bitmap.getWidth() < maxImageSize && bitmap.getHeight() < maxImageSize)) {
                    bitmap = ImageResizer.scaleDown(bitmap, maxImageSize, false);
                }
                // resize image then upload to server
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
            byte[] byteProfileImage = bytes.toByteArray();

            if (byteProfileImage != null) {
                profile_image.setImageBitmap(bitmap);
                final StorageReference ref = storageReference.child(FirebaseTables.profileImageTable).child(firebaseUser.getUid() + ".jpg");
                UploadTask uploadTask = ref.putBytes(byteProfileImage);
                uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            progressDialogGenerator.cancel();
                            Toast.makeText(getContext(), "Resim Yüklenmedi", Toast.LENGTH_SHORT).show();
                            return null;
                        }
                        return ref.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            progressDialogGenerator.cancel();
                            Toast.makeText(getContext(), "Resim Yüklendi", Toast.LENGTH_SHORT).show();
                            String image = task.getResult().toString();
                            progressDialogGenerator.createProgress("Resim Güncelleniyor...", false);
                            updateUserImage(image);
                        } else {
                            if (task.getResult() == null)
                                progressDialogGenerator.cancel();
                            Toast.makeText(getContext(), "Resim Yüklenmedi", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
                Toast.makeText(getContext(), "Resim yüklenirken bir problem oluştu.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void updateUserImage(final String image) {
        databaseReference.child("image").setValue(image).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getContext(), "Resim güncellendi!", Toast.LENGTH_SHORT).show();
                    imageUrl = image;
                } else {
                    Toast.makeText(getContext(), "Resim güncellenemedi!", Toast.LENGTH_SHORT).show();
                }
                progressDialogGenerator.cancel();
            }
        });
    }

    private void signOut() {
        progressDialogGenerator.createProgress("Çıkış yapılıyor...", false);
        auth.signOut();
        progressDialogGenerator.cancel();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        getActivity().finish();
    }

    private void getUserInformation() {
        progressDialogGenerator.createProgress("Bilgilerinize ulaşılıyor", false);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progressDialogGenerator.cancel();
                if (dataSnapshot.exists()) {
                    User user = dataSnapshot.getValue(User.class);
                    Log.i("USER", user.toString());

                    imageUrl = user.getImage();
                    oldName = user.getName();
                    oldTelephone = user.getTelephone();
                    oldEducation = user.getEducation();
                    oldBirthdate = user.getBirthdate();
                    oldSummary = user.getSummary();
                    noOfFriends = user.getNumberOfFriends();

                    if (oldName.equals("")) {
                        nameEditText.setHint("Not specified");
                    }

                    if (oldTelephone.equals("")) {
                        phoneEditText.setHint("Not specified");
                    }

                    if (oldEducation.equals("")) {
                        educationEditText.setHint("Not specified");
                    }

                    if (oldBirthdate.equals("")) {
                        birhdateEditText.setHint("Not specified");
                    }

                    if (oldSummary.equals("")) {
                        summaryEditText.setHint("Not specified");
                    }

                    nameEditText.setText(oldName);
                    phoneEditText.setText(oldTelephone);
                    educationEditText.setText(oldEducation);
                    birhdateEditText.setText(oldBirthdate);
                    summaryEditText.setText(oldSummary);

                    if (!imageUrl.equals("null")) {
                        Picasso.get().load(imageUrl).placeholder(R.drawable.default_profile_male).into(profile_image);
                    }

                } else {
                    Toast.makeText(getActivity(), "Bilgilere ulaşılamadı...", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressDialogGenerator.cancel();
            }
        });
    }

    private void updateUserInformation(final String name, final String telephone, String education, String birthdate, String summary) {
        Map map = new HashMap();
        map.put(FirebaseTables.userTableName, name);
        map.put(FirebaseTables.userTableTelephone, telephone);
        map.put(FirebaseTables.userTableEducation, education);
        map.put(FirebaseTables.userTableBirthdate, birthdate);
        map.put(FirebaseTables.userTableSummary, summary);
        map.put(FirebaseTables.userTableImage, imageUrl);
        map.put(FirebaseTables.userTableNoOfFriends, noOfFriends);

        progressDialogGenerator.createProgress("Bilgiler güncelleniyor...", false);
        databaseReference.setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    databaseReference.child(FirebaseTables.userTableTelephone).setValue(telephone).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                progressDialogGenerator.cancel();
                                Toast.makeText(getContext(), "Bilgiler Güncellendi...", Toast.LENGTH_SHORT).show();
                                ChangeFragment changeFragment = new ChangeFragment(getContext());
                                changeFragment.change(new MainPageFragment(), R.id.mainFragment);
                            } else {
                                progressDialogGenerator.cancel();
                                Toast.makeText(getContext(), "Telefon bilgisi güncellenemedi...", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    progressDialogGenerator.cancel();
                    Toast.makeText(getContext(), "İsim bilgisi güncellenemedi...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
